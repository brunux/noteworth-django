"""
Providers tasks.
"""
import logging
from hashlib import sha256

import requests
from celery import shared_task
from django.db import transaction
from providers.models import Provider

logger = logging.getLogger(__name__)


class ProviderAPI:
    """
    Handlers for the provider API.
    """
    host = 'http://127.0.0.1:5000'
    token_header = 'Super-Secure-Token'
    request_tries = 0
    request_max_tries = 2

    def __init__(self):
        self.token_seed = None
        self.tokens = {}

    def _log_and_retry(self, error, timeout, callback, endpoint):
        logger.error(error)
        self.request_tries += 1

        if self.request_tries <= self.request_max_tries:
            return callback(timeout=timeout + timeout)

        raise Exception(
            f'ProviderAPI: Unable to get response {endpoint} max tries '
            f'{self.request_max_tries} reached'
        )

    def auth(self, endpoint='/auth', timeout=1):
        """
        Main entry point for authentication.
        """
        try:
            resp = requests.get(f"{self.host}{endpoint}", timeout=timeout)
        except Exception as e:
            return self._log_and_retry(str(e), timeout, self.auth, endpoint)

        if 200 <= resp.status_code < 300:
            token_seed = resp.headers.get(self.token_header, None)

            if token_seed is None:
                error = (
                    f'ProviderAPI: Unable to get {self.token_header} header'
                    f'tries: {self.request_tries}'
                )
                return self._log_and_retry(error, timeout, self.auth, endpoint)

            self.request_tries = 0
            return token_seed

        error = (
            f'ProviderAPI: Invalid response {resp} tries: '
            f'{self.request_tries}'
        )
        return self._log_and_retry(error, timeout, self.auth, endpoint)

    def get_token(self, endpoint):
        """
        sha256 a token and saves it to a cache.
        """
        if not self.tokens.get(endpoint, False):
            token = sha256(
                f"{self.token_seed}{endpoint}".encode('utf-8')
            ).hexdigest()
            self.tokens[endpoint] = token
            return token
        return self.tokens[endpoint]

    def provider_to_model_instance(self, provider):
        """
        Validates provider data.

        Optionally we could use DRF serializer to get detailed field errors.
        """
        try:
            provider = Provider(**provider)
            provider.full_clean()
        except Exception as e:
            logger.error(
                f"ProviderAPI: Invalid provider {provider} error {e}"
            )
            return None
        return provider

    @transaction.atomic
    def commit_providers_bulk(self, providers):
        """
        Commits providers to DB all at once.
        """
        logger.info(
            "ProviderAPI: Trying to commit providers in bulk"
        )
        provider_instances = [
            self.provider_to_model_instance(provider) for provider
            in providers
            if provider
        ]
        if not provider_instances:
            return False
        try:
            result = Provider.objects.bulk_create(provider_instances)
        except Exception as e:
            logger.error(
                f"ProviderAPI: Unable to bulk create provider error {e} "
                f"while processing providers {providers}"
            )
            return False
        return result

    def commit_providers_one_by_one(self, providers):
        """
        Unoptimized provider creation one by one.

        Second effort to commit providers to DB.
        """
        logger.info(
            "ProviderAPI: Trying to commit providers one by one"
        )
        success = []
        for provider in providers:
            provider_instance = self.provider_to_model_instance(provider)

            if not provider_instance:
                continue
            try:
                provider_instance.save()
            except Exception as e:
                logger.error(
                    f"ProviderAPI: Unable to save provider error {e} "
                    f"while processing provider {provider}"
                )
            success.append(provider_instance)

        return success

    def get_providers(self, endpoint='/providers', timeout=1):
        """
        Main entry point to get providers.
        """
        if not self.token_seed:
            self.token_seed = self.auth()

        try:
            resp = requests.get(
                f"{self.host}{endpoint}",
                timeout=timeout,
                headers={'X-Request-Checksum': self.get_token(endpoint)}
            )
        except Exception as e:
            return self._log_and_retry(
                str(e), timeout, self.get_providers, endpoint
            )

        if 200 <= resp.status_code < 300:
            try:
                providers = resp.json()
            except Exception as e:
                return self._log_and_retry(
                    str(e), timeout, self.get_providers, endpoint
                )

            if not providers or not providers['providers']:
                error = (
                    f'Empty `providers` response {providers}'
                    f'tries: {self.request_tries}'
                )
                return self._log_and_retry(
                    error, timeout, self.get_providers, endpoint
                )

            success = self.commit_providers_bulk(providers['providers'])
            if not success:
                success = self.commit_providers_one_by_one(
                    providers['providers']
                )

            self.request_tries = 0
            return success

        error = f'Invalid response {resp} tries: {self.request_tries}'
        return self._log_and_retry(
            error, timeout, self.get_providers, endpoint
        )


@shared_task
def providers_sync():
    """
    Initiates sync to API.
    """
    logger.info(
        'providers_sync: Starting `Provider` API Sync'
    )
    api = ProviderAPI()
    success = api.get_providers()
    if success:
        logger.info(
            f"providers_sync: Successfully loaded {len(success)} providers"
        )
        return len(success)
    return 0
