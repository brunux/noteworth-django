import logging

from celery.result import AsyncResult
from django.core.management.base import BaseCommand
from providers.models import Provider
from providers.tasks import providers_sync

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Syncs `Providers` from API.'

    @staticmethod
    def get_task_status(task_id):
        try:
            task_result = AsyncResult(task_id)
            status = {
                'task_id': task_id,
                'task_status': task_result.status,
                'task_result': task_result.result
            }
        except Exception as e:
            logger.error(
                f"ProvidersSync: Unexpected error {e} while getting "
                f"tasks {task_id} status."
            )
            return None
        return status

    def add_arguments(self, parser):
        parser.add_argument(
            '--delete',
            action='store_true',
            help='Delete current `Providers` model instances.',
        )

        parser.add_argument(
            '--sync',
            action='store_true',
            help='Run synchronously instead of async.',
        )

        parser.add_argument(
            '--check',
            action='store',
            help='Check status of async task with an ID',
        )

    def handle(self, *args, **options):
        """
        Runs the provider sync or async.
        """
        if options.get('check'):
            status = self.get_task_status(options['check'])
            self.stdout.write(self.style.NOTICE(str(status)))
            return None

        if options['delete']:
            try:
                Provider.objects.all().delete()
            except Exception as e:
                logger.error(
                    f"ProvidersSync: Unable to delete `Providers` error {e}"
                )
                return None
            logger.info('ProvidersSync: Successfully deleted `Provider`')

        if options['sync']:
            providers_sync()
            return None

        task = providers_sync.delay()
        self.stdout.write(
            self.style.NOTICE(
                f"ProvidersSync: Async task initialized check status with:\n"
                f"python manage.py providers_sync --check {task}"
            )
        )
