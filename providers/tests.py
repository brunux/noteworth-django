"""
coverage run -m pytest

Some test_providerAPI* test need mocking to behave like integration tests.
"""

import pytest
from celery.result import EagerResult
from django.test import RequestFactory
from providers.tasks import providers_sync

pytestmark = pytest.mark.django_db


def test_provider_commad_run_async(settings):
    settings.CELERY_TASK_ALWAYS_EAGER = True
    task_result = providers_sync.delay()
    assert isinstance(task_result, EagerResult)


def test_provider_commad_delete():
    pass


def test_provider_commad_check_state():
    pass


def test_provider_commad_run_sync():
    pass


def test_providerAPI_get_token():
    pass


def test_providerAPI_provider_to_model_instance():
    pass


def test_providerAPI_commit_in_bulk():
    pass


def test_providerAPI_commit_one_by_one():
    pass


def test_providerAPI_auth_should_fail_max_two_times():
    pass


def test_providerAPI_auth_should_succed():
    pass


def test_providerAPI_providers_should_fail_with_500():
    pass


def test_providerAPI_providers_should_fail_with_time_out():
    pass


def test_providerAPI_providers_should_fail_json_reponse():
    pass


def test_providerAPI_providers_should_fail_max_two_times():
    pass


def test_providerAPI_should_create_10():
    pass


def test_providerAPI_providers_should_create_5_of_10():
    pass


def test_providerAPI_providers_should_not_create():
    pass
