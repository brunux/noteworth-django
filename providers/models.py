from django.db import models
from model_utils.models import TimeStampedModel


class Provider(TimeStampedModel):
    """
    """
    name_given = models.CharField(max_length=254)
    name_family = models.CharField(max_length=254)
    title = models.CharField(max_length=50)
    clinic = models.CharField(max_length=254)

    class Meta:
        unique_together = [
            ['name_given', 'name_family', 'title', 'clinic']
        ]
        ordering = ['-created', 'name_family', 'name_given']

    def __str__(self):
        return f"{self.name_family}, {self.name_given} <{self.id}>"
