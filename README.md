# Noteworth Challenge
This is a code challenge based on [noteworth-challenge-api](https://github.com/datamindedsolutions/noteworth-challenge-api)

## The goal
Sync **providers** from an API.

## Solution description
Create a configurable command to get the **providers** from the API, this in the
form of async tasks which also could be invoked directly within python code,
so in the future if needed, this task could be demonized (systemd) or
scheduled (celery).

It should authenticate and get items programmatically validating the
response and possible errors if any, retry request a max of 2 times.

## Assumptions
- Data is always consistence.
- Providers should be unique with:

```python
['name_given', 'name_family', 'title', 'clinic']
```

## Project description
This project uses Django which provide many tools, in this case use the
commands utilities for launching and configure the **providers** sync task.

### Stack
Postgres: Hight reliable DB engine, plays nice with Django.

Celery: Task queue which includes many features like scheduling.

Redis: Performant in memory key-value data store which can be used as task
broker and cache service.

### Project Setup
- Create a virtual environment and activate it.
- Install dependencies from `requirements.txt`
- Run Postgres, Celery and Redis.

```bash
docker run --name noteworth-postgres -e POSTGRES_DB=noteworth_db -e POSTGRES_USER=debug -e POSTGRES_PASSWORD=debug -d postgres -p 5432:5432
```

```bash
celery worker --app=noteworth --loglevel=info --logfile=logs/celery.log
```

```bash
docker run --name noteworth-redis -d redis -p 6379:6379
```
- Run migrations.
- Run sync command to lunch API sync on the background.
- Create superuser.
- Explore results on admin.

### Command usage
```bash
❯ python manage.py help providers_sync
usage: manage.py providers_sync [-h] [--delete] [--sync] [--check CHECK] [--version]
                                [-v {0,1,2,3}] [--settings SETTINGS]
                                [--pythonpath PYTHONPATH] [--traceback] [--no-color]
                                [--force-color] [--skip-checks]

Syncs `Providers` from API.

optional arguments:
  -h, --help            show this help message and exit
  --delete              Delete current `Providers` model instances.
  --sync                Run synchronously instead of async.
  --check CHECK         Check status of async task with an ID
  --version             show program's version number and exit
  -v {0,1,2,3}, --verbosity {0,1,2,3}
                        Verbosity level; 0=minimal output, 1=normal output,
                        2=verbose output, 3=very verbose output
  --settings SETTINGS   The Python path to a settings module, e.g.
                        "myproject.settings.main". If this isn't provided, the
                        DJANGO_SETTINGS_MODULE environment variable will be used.
  --pythonpath PYTHONPATH
                        A directory to add to the Python path, e.g.
                        "/home/djangoprojects/myproject".
  --traceback           Raise on CommandError exceptions
  --no-color            Don't colorize the command output.
  --force-color         Force colorization of the command output.
  --skip-checks         Skip system checks.
```

**Get providers from API asynchronously**

```bash
❯ python manage.py providers_sync
```

**Check async task status**

```bash
❯ python manage.py providers_sync --check the-id-abc-123
```

**Get providers from API synchronously**

```bash
❯ python manage.py providers_sync --sync
```

**Delete and get providers**

```bash
❯ python manage.py providers_sync --delete
```

### Improvements
- Add integration like tests by mocking API responses with edge cases.
- Keep record of consumed items (state) to use delta like for data synchronization.
- Improve project layout, requirements.txt, settings, secrets.
- Add development apps e.g. django-extension.
- Add timing logs.

### Test
Using `pytest` for simplicity, could be mix/extend with Django test utilities.
```bash
❯ coverage run -m pytest
❯ coverage report
❯ coverage html
```
Check `pytest.ini` for default conf.

### Lint
Manually run flake8 to lint you code.
```bash
❯ flake8 .
```
Check `setup.cfg` for other lint options.

### TODO
- Finish the tests.
- Add tox to test and lint code.
